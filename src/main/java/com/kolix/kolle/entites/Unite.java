
package com.kolix.kolle.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;
/*
 *
 * @author koiranga
 */
@Entity
@Data
public class Unite implements Serializable {
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    
    @Column(name="_codeUE", nullable=false)
    private String codeUE;
    
    @Column(name="_intitule", nullable=false)
    private String intitule;
    
    @Column(name="_niveau", nullable=false)
    private int niveau;
    
    @Column(name="_semestre", nullable=false)
    private int semestre;
    
    @Column(name="_option", nullable=false)
    private String option;
    
    @Column(name="_parcours", nullable=false)
    private String parcours;
    
    @JsonIgnore
    @XmlTransient
    
    @OneToMany
    private List<Evaluation> evaluation;
    
}

