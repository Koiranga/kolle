
package com.kolix.kolle.entites;

import com.sun.istack.NotNull;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

/**
 *
 * @author koiranga
 */
@Entity
@Data
public class User implements Serializable{
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idUser;
    
    @NotNull
    private String nom;
    
    @NotNull
    private String password;
   }


