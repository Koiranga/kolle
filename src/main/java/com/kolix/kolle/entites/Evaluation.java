
package com.kolix.kolle.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlTransient;
import lombok.Data;

/**
 *
 * @author koiranga
 */
@Entity
@Data
public class Evaluation implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEva;
    
    @Column(name= "typeEva",nullable = false)
    private String typeEva;
     
    @Column(name= "nbreEtudiant",nullable = false)
    private int nbreEtudiant;
    
    @Column(name= "rapport",nullable = false)
    private String rapport;
   
    @Column(name= "nbreCopie",nullable = false)
    private int nbreCopie;
    
    @Column(name= "session",nullable = false)
    private String session;
     
    @Column(name= "dateEva",nullable = false)
     @Temporal(TemporalType.DATE)
    private Date dateEva;
    
      @JsonIgnore
    @XmlTransient
      
    @ManyToOne
      @JoinColumn(name="unite", nullable=false)
    private Unite unite;
    
    @ManyToMany
    @JoinTable(name="assistant",joinColumns=@JoinColumn(name="idEva"),inverseJoinColumns=@JoinColumn(name="idAssistant"))
    private List<Assistant> assistant;
    
    @ManyToOne
    @JoinColumn(name="anneeAcademique", nullable=false)
    private AnneeAcademique anneeAcademique;
    
    @ManyToOne
    @JoinColumn(name="salle", nullable=false)
    private Salle salle;

}
