
package com.kolix.kolle.entites;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author koiranga
 */
@Entity
public class Enseignant extends Assistant{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name="_grade",nullable = false)
    private String grade;
    
    @Column(name="_fonctionEns",nullable = false)
    private String fonctionEns;
}

