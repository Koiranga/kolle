/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services;

import com.kolix.kolle.entites.Salle;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author koiranga
 */
@Path("/salles")
public interface ISalleResource {
    
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public ResponseEntity<Page<Salle>> getAllSalle(@QueryParam("page") @DefaultValue("0")int page, @QueryParam("size") @DefaultValue("20")int pagesize);
    
    @GET
    @Path("{numSalle : \\numSalle+}")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public ResponseEntity<Salle> findSalle(@PathParam("numSalle")String numSalle);
    
    @PUT
    @Path("{numSalle: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Salle> updateSalle(@PathParam("numSalle") String numSalle, Salle salle);
    
    
    @POST
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Salle> addSalle(Salle salle);
    
    @DELETE
    @Path("{numSalle: \\d+}")
    public void deleteSalle(@PathParam("numSalle") String numSalle);
    
}
