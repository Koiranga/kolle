/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services;

import com.kolix.kolle.services.impl.AnneeAcademiqueResource;
import com.kolix.kolle.services.impl.AssistantResource;
import com.kolix.kolle.services.impl.EvaluationResource;
import com.kolix.kolle.services.impl.SalleResource;
import com.kolix.kolle.services.impl.UniteResource;
import com.kolix.kolle.services.impl.UserResource;
import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 *
 * @author koiranga
 */
@Component
@ApplicationPath("/api")

public class ApplicationConfig extends ResourceConfig{
    
      public ApplicationConfig(){
        register(UniteResource.class);
        register(UserResource.class);
        register(EvaluationResource.class);
        register(SalleResource.class);
        register(AssistantResource.class);
        register(AnneeAcademiqueResource.class);
    }
}
