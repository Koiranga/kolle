/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services;

import com.kolix.kolle.entites.Unite;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;


/**
 *
 * @author koiranga
 */
@Path("/unites")

public interface IUniteResource {
    
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public ResponseEntity<Page<Unite>> getAllUnites(@QueryParam("page") @DefaultValue("0")int page, @QueryParam("size") @DefaultValue("20")int pagesize);
    
    @GET
    @Path("{id : \\d+}")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public ResponseEntity<Unite> findUnite(@PathParam("id")long id);
    
    @GET
    @Path("/search")
    @Produces(value = {MediaType.APPLICATION_JSON})
     public ResponseEntity<Unite> searchUnite(@QueryParam("codeUE")String codeUE);
    
    @PUT
    @Path("{id: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Unite> updateUnite(@PathParam("id") long id, Unite unite);
    
    
    @POST
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Unite> addUnite(Unite unite);
    
    @DELETE
    @Path("{id: \\d+}")
    public void deleteUnite(@PathParam("id") long id);
  
   }
