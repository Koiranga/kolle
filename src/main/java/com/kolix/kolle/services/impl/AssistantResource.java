/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services.impl;

import com.kolix.kolle.dao.AssistantDao;
import com.kolix.kolle.entites.Assistant;
import com.kolix.kolle.services.IAssistantResource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author koiranga
 */
public class AssistantResource implements IAssistantResource{

     @Autowired
    private AssistantDao assistantDao;


    @Override
    public Page<Assistant> getAllAssistant(int page, int pagesize){
         return assistantDao.findAll(PageRequest.of(page, pagesize));
        
    }

    @Override
    public Page<Assistant> searchAssistant(String nom, int page, int pagesize) {
          return assistantDao.findByNomLikeIgnoreCase("%" + nom + "%", PageRequest.of(page, pagesize));

    }

    @Override
    public ResponseEntity<Assistant> findAssistant(long id) {
          Optional<Assistant> assistant = assistantDao.findById(id);
             if (assistant.isPresent()) {
            return ResponseEntity.ok(assistant.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<Assistant> updateAssistant(long id, Assistant assistant) {
        Optional<Assistant> assist = assistantDao.findById(id);
        if (!assist.isPresent()) {
            Assistant c = assist.get();
        c.setNom(assistant.getNom());
        c.setPrenom(assistant.getPrenom());
        assistantDao.save(c);
        return ResponseEntity.ok(c); 
        } else{
         return ResponseEntity.notFound().build();
          }
        
    }

    @Override
    public ResponseEntity<Assistant> addAssistant(Assistant assistant) {
         Assistant c = assistantDao.save(assistant);
         URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{idAssistant}")
                .buildAndExpand(c.getId())
                .toUri();
        return ResponseEntity.created(location).body(c);
    }

    @Override
    public void deleteAssistant(long idAssistant) {
          assistantDao.deleteById(idAssistant);
    }
 /* @Override
    public Page<Assistant> getAllAssistant(int page, int pagesize) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
}
