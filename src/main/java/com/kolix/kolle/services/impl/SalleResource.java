/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services.impl;

import com.kolix.kolle.dao.SalleDao;
import com.kolix.kolle.entites.Salle;
import com.kolix.kolle.services.ISalleResource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author koiranga
 */
public class SalleResource implements ISalleResource{
    
     @Autowired
    private SalleDao salleDao;

    @Override
    public ResponseEntity<Page<Salle>> getAllSalle(int page, int pagesize) {
      return (ResponseEntity<Page<Salle>>) salleDao.findAll(PageRequest.of(page, pagesize));   
    }

    @Override
    public ResponseEntity<Salle> findSalle(String numSalle) {
          Optional<Salle> salle = salleDao.findById(numSalle);
             if (salle.isPresent()) {
            return ResponseEntity.ok(salle.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<Salle> updateSalle(String numSalle, Salle salle) {
       Optional<Salle> sal = salleDao.findById(numSalle);
        if (!sal.isPresent()) {
            Salle c = sal.get();
        c.setNbrePlace(salle.getNbrePlace());
        salleDao.save(c);
        return ResponseEntity.ok(c); 
        } else{
         return ResponseEntity.notFound().build();
          } 
    }

    @Override
    public ResponseEntity<Salle> addSalle(Salle salle) {
         Salle c = salleDao.save(salle);
         URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{idAssistant}")
                .buildAndExpand(c.getNumSalle())
                .toUri();
        return ResponseEntity.created(location).body(c);
    }

    @Override
    public void deleteSalle(String numSalle) {
          salleDao.deleteById(numSalle);
    }
    
}

