/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services.impl;

import com.kolix.kolle.dao.UserDao;
import com.kolix.kolle.entites.User;
import com.kolix.kolle.services.IUserResource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author koiranga
 */
public class UserResource implements IUserResource{
    
    @Autowired
     private UserDao userDao;

    @Override
    public ResponseEntity<User> addUser(User user) {
 User c = userDao.save(user);
 
         URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{idUser}")
                .buildAndExpand(c.getIdUser())
                .toUri();
        return ResponseEntity.created(location).body(c);  
    }

    @Override
    public ResponseEntity<User> findUser(long idUser) {
    Optional<User> user = userDao.findById(idUser);
             if (user.isPresent()) {
            return ResponseEntity.ok(user.get());
        }
        return ResponseEntity.notFound().build();  
    }

    @Override
    public ResponseEntity<User> updateUser(long idUser, User user) {
 Optional<User> use = userDao.findById(idUser);
        if (!use.isPresent()) {
            User c = use.get();
        c.setNom(user.getNom());
        c.setPassword(user.getPassword());
        userDao.save(c);
        return ResponseEntity.ok(c); 
        } else{
         return ResponseEntity.notFound().build();
          }    }

    @Override
    public void deleteUser(long idUser) {
          userDao.deleteById(idUser);
    }
    
}


