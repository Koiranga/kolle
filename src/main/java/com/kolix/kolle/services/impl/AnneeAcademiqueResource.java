/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services.impl;

import com.kolix.kolle.dao.AnneeAcademiqueDao;
import com.kolix.kolle.entites.AnneeAcademique;
import com.kolix.kolle.dao.EvaluationDao;
import com.kolix.kolle.services.IAnneeAcademiqueResource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author koiranga
 */
public class AnneeAcademiqueResource implements IAnneeAcademiqueResource{
    
     @Autowired
    private AnneeAcademiqueDao anneeAcademiqueDao;

    @Autowired
    private EvaluationDao evaluationDao;
    private long idEva;
    
    @Override
    public Page<AnneeAcademique> getAllAnneeAcademique(int page, int pagesize) {
       return anneeAcademiqueDao.findAll(PageRequest.of(page, pagesize));   
    }

    @Override
    public Page<AnneeAcademique> searchAnneeAcademique(long anneeAca, int page, int pagesize) {
        return anneeAcademiqueDao.findByanneeAca("%" + anneeAca + "%", PageRequest.of(page, pagesize));
    }

    public ResponseEntity<AnneeAcademique> findAnneeAcademique(Long idAca) {
        Optional<AnneeAcademique> anneeAcademique = anneeAcademiqueDao.findById(idAca);
        if (anneeAcademique.isPresent()) {
            return ResponseEntity.ok(anneeAcademique.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<AnneeAcademique> updateAnneeAcademique(long idAca, AnneeAcademique anneeAcademique) {
        Optional<AnneeAcademique> aca = anneeAcademiqueDao.findById(idAca);
        if (!aca.isPresent()) {
            AnneeAcademique c = aca.get();
        c.setAnneeAca(anneeAcademique.getAnneeAca());
        anneeAcademiqueDao.save(c);
        return ResponseEntity.ok(c); 
        } else{
         return ResponseEntity.notFound().build();
          }
    }

    @Override
    public ResponseEntity<AnneeAcademique> addAnneeAcademique(AnneeAcademique anneeAcademique) {
        AnneeAcademique c = anneeAcademiqueDao.save(anneeAcademique);
       URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(c.getIdAca())
                .toUri();
        return ResponseEntity.created(location).body(c);
   
    }

    @Override
    public void deleteAnneeAcademique(long idAca) {
        anneeAcademiqueDao.deleteById(idAca);
    }


}
