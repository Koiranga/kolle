/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services.impl;

import com.kolix.kolle.dao.UniteDao;
import com.kolix.kolle.entites.Unite;
import com.kolix.kolle.services.IUniteResource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author koiranga
 */
public class UniteResource implements IUniteResource{
    
     @Autowired
        private UniteDao uniteDao;


    @Override
    public ResponseEntity<Page<Unite>> getAllUnites(int page, int pagesize) {
         return (ResponseEntity<Page<Unite>>) uniteDao.findAll(PageRequest.of(page, pagesize));
    }

    @Override
    public ResponseEntity<Unite> findUnite(long id) {
          Optional<Unite> unite = uniteDao.findById(id);
             if (unite.isPresent()) {
            return ResponseEntity.ok(unite.get());
        }
        return ResponseEntity.notFound().build();
    }

    @Override
    public ResponseEntity<Unite> searchUnite(String codeUE) {
          return uniteDao.findByCodeUELikeIgnoreCase("%" + codeUE + "%");
    }

    @Override
    public ResponseEntity<Unite> updateUnite(long id, Unite unite) {
        Optional<Unite> ue = uniteDao.findById(id);
        if (!ue.isPresent()) {
            Unite c = ue.get();
        c.setCodeUE(unite.getCodeUE());
        c.setIntitule(unite.getIntitule());
        c.setNiveau(unite.getNiveau());
        c.setSemestre(unite.getSemestre());
        c.setParcours(unite.getParcours());
        c.setOption(unite.getOption());
        uniteDao.save(c);
        return ResponseEntity.ok(c); 
        } else{
         return ResponseEntity.notFound().build();
          }
           }

    @Override
    public ResponseEntity<Unite> addUnite(Unite unite) {
         Unite c = uniteDao.save(unite);
         URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{idAssistant}")
                .buildAndExpand(c.getId())
                .toUri();
        return ResponseEntity.created(location).body(c);    }

    @Override
    public void deleteUnite(long id) {
          uniteDao.deleteById(id);
    }
    
}
