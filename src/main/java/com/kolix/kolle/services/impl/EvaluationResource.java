
package com.kolix.kolle.services.impl;

import com.kolix.kolle.dao.EvaluationDao;
import com.kolix.kolle.entites.Evaluation;
import com.kolix.kolle.services.IEvaluationResource;
import java.net.URI;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;




/**
 *
 * @author koiranga
 */
public class EvaluationResource implements IEvaluationResource{

    
     @Autowired
    private EvaluationDao evaluationDao;

    @Override
    public Page<Evaluation> getAllEvaluation(int page, int pagesize) {
         return evaluationDao.findAll(PageRequest.of(page, pagesize));
    }
/*
    @Override
     public Page<Evaluation> searchEvaluation(String typeEva, int page, int pagesize){
       return evaluationDao.findByTypeEvaLikeIgnoreCase("%" + typeEva + "%", "%" + (PageRequest.of(page, pagesize)) + "%");
    }
*/
    @Override
    public ResponseEntity<Evaluation> findEvaluation(long idEva) {
         Optional<Evaluation> evaluation = evaluationDao.findById(idEva);
             if (evaluation.isPresent()) {
            return ResponseEntity.ok(evaluation.get());
        }
        return ResponseEntity.notFound().build();    }

    @Override
    public ResponseEntity<Evaluation> updateEvaluation(long idEva, Evaluation evaluation) {
 Optional<Evaluation> eva = evaluationDao.findById(idEva);
        if (!eva.isPresent()) {
            Evaluation c = eva.get();
        c.setTypeEva(evaluation.getTypeEva());
        c.setSession(evaluation.getSession());
        c.setRapport(evaluation.getRapport());
        c.setNbreCopie(evaluation.getNbreCopie());
        c.setNbreEtudiant(evaluation.getNbreEtudiant());
        c.setDateEva(evaluation.getDateEva());
        evaluationDao.save(c);
        return ResponseEntity.ok(c); 
        } else{
         return ResponseEntity.notFound().build();
          }    }

    @Override
    public ResponseEntity<Evaluation> addEvaluation(Evaluation evaluation) {
         Evaluation eva = evaluationDao.save(evaluation);
         URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{idAssistant}")
                .buildAndExpand(eva.getIdEva())
                .toUri();
        return ResponseEntity.created(location).body(eva);    
    }   
   
}
