/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services;

import com.kolix.kolle.entites.AnneeAcademique;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author koiranga
 */
@Path("/anneeacademiques")
public interface IAnneeAcademiqueResource {
    
     @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<AnneeAcademique> getAllAnneeAcademique(@DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
    
    
    @GET
    @Path("/search")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<AnneeAcademique> searchAnneeAcademique(@QueryParam("anneeAca")long anneeAca, @DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
    
    @PUT
    @Path("{idAca: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<AnneeAcademique> updateAnneeAcademique(@PathParam("idAca") long idAca, AnneeAcademique anneeAcademique);
     
    @POST
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<AnneeAcademique> addAnneeAcademique(AnneeAcademique anneeAcademique);
    
    @DELETE
    @Path("{idAca: \\d+}")
    public void deleteAnneeAcademique(@PathParam("idAca") long idAca);
   
}
