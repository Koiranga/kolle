/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services;

import com.kolix.kolle.entites.Assistant;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author koiranga
 */
@Path("/assistants")
public interface IAssistantResource {
    
     @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Assistant> getAllAssistant(@DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
    
    @GET
    @Path("/search")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Assistant> searchAssistant(@QueryParam("nom")String nom, @DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
    
    @GET
    @Path("{id: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Assistant> findAssistant(@PathParam("id") long id);
    
    @PUT
    @Path("{id: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Assistant> updateAssistant(@PathParam("id") long id, Assistant assistant);
    
    
    @POST
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Assistant> addAssistant(Assistant assistant);
    
    @DELETE
    @Path("{id: \\d+}")
    public void deleteAssistant(@PathParam("id") long id);
    
    
}
