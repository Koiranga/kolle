/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.services;

import com.kolix.kolle.entites.User;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author koiranga
 */
@Path("/users")
public interface IUserResource { 
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public ResponseEntity<User> addUser(User user);
    
    @GET
    @Path("idUser: \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<User> findUser(@PathParam("idUser") long idUser);
    
    @PUT
    @Path("{idUser: \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public ResponseEntity<User> updateUser(@PathParam("idUser") long idUser, User user);
    
    @DELETE
    @Path("{idUser: \\d+}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteUser(@PathParam("idUser") long idUser);
}
