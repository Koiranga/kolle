
package com.kolix.kolle.services;

import com.kolix.kolle.entites.Evaluation;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author koiranga
 */
@Path("/evaluations")
public interface IEvaluationResource {
    
    @GET
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Evaluation> getAllEvaluation(@DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
/*
    @GET
    @Path("/search")
    @Produces(value = {MediaType.APPLICATION_JSON})
    public Page<Evaluation> searchEvaluation(@QueryParam("typeEva")String typeEva, @DefaultValue("0") @QueryParam("page")int page, @DefaultValue("20") @QueryParam("size")int pagesize);
    */
    @GET
    @Path("{idEva: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Evaluation> findEvaluation(@PathParam("idEva") long idEva);
    
    @PUT
    @Path("{idEva: \\d+}")
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Evaluation> updateEvaluation(@PathParam("idEva") long idEva, Evaluation evaluation);
    
    
    @POST
    @Produces(value ={MediaType.APPLICATION_JSON})
    @Consumes(value ={MediaType.APPLICATION_JSON})
    public ResponseEntity<Evaluation> addEvaluation(Evaluation evaluation);
    
   
}
