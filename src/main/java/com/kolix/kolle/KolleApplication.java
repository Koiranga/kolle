package com.kolix.kolle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KolleApplication {

	public static void main(String[] args) {
		SpringApplication.run(KolleApplication.class, args);
	}

}
