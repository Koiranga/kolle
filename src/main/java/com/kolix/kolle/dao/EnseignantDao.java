
package com.kolix.kolle.dao;

import com.kolix.kolle.entites.Enseignant;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author koiranga
 */
public interface EnseignantDao extends JpaRepository<Enseignant, Long>{
    
}

