/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.dao;

import com.kolix.kolle.entites.Salle;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author koiranga
 */
public interface SalleDao extends JpaRepository<Salle, String>{
    
     public List<Salle> findBynumSalle(String numSalle);
}
