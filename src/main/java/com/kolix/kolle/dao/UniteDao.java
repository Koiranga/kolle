/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.dao;

import com.kolix.kolle.entites.Unite;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;

/**
 *
 * @author koiranga
 */
public interface UniteDao extends JpaRepository<Unite, Long>{
    
  public Page<Unite> findByCodeUE(String codeUE, Pageable page);
  public Page<Unite> findByIntituleLikeIgnoreCase(String intitule, Pageable page);

  public ResponseEntity<Unite> findByCodeUELikeIgnoreCase(String string);
    
}
