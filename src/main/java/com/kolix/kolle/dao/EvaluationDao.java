
package com.kolix.kolle.dao;

import com.kolix.kolle.entites.Evaluation;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author koiranga
 */
public interface EvaluationDao extends JpaRepository<Evaluation,Long>{
 
    public List<Evaluation> findByidEva(long idEva);
    public List<Evaluation> findByTypeEvaLike(String typeEva);
    public Page<Evaluation> findByTypeEvaIgnoreCase(String typeEva, Pageable page);

   // public Page<Evaluation> findByTypeEvaLikeIgnoreCase(String string, String string0);
    
}

