/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kolix.kolle.dao;


import com.kolix.kolle.entites.AnneeAcademique;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author koiranga
 */
public interface AnneeAcademiqueDao extends JpaRepository <AnneeAcademique, Long>{
    
    public List<AnneeAcademique> findByidAca(Long idAca);

    public Page<AnneeAcademique> findByanneeAca(String anneeAca, Pageable page);
}

