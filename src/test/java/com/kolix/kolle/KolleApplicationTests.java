package com.kolix.kolle;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
@SpringBootApplication
@SpringBootTest
class KolleApplicationTests {

	@Test
	void contextLoads() {
	}
        
	public static void main(String[] args) {
		SpringApplication.run(KolleApplication.class, args);
	}


}
